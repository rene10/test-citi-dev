//
//  SceneDelegate.h
//  Test
//
//  Created by Rene Cabañas on 17/11/20.
//



#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

