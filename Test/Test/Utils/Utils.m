//
//  Utils.m
//  Test
//
//  Created by Admin on 17/11/20.
//  Copyright © 2020 Rene Cabañas. All rights reserved.
//

#import "Utils.h"
#import "Reachability/Reachability.h"

#define CONCT_KM           @"%@ km"
#define METERS_INTO_KM     1000
#define VALUE_NULL         @"(null)"
#define VALUE_BREAK_LINE   @"\n"
#define CONCT_METERS       @"%@ metros"

@implementation Utils

- (NSString *)stringWithDistance:(double)distance {
    NSString *format;
    if (distance < METERS_INTO_KM) {
        format = CONCT_METERS;
    } else {
        format = CONCT_KM;
        distance = distance / METERS_INTO_KM;
    }

    return [NSString stringWithFormat:format, [self stringWithDouble:distance]];
}

- (NSString *)stringWithDouble:(double)value {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[NSLocale currentLocale]];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:1];
    return [numberFormatter stringFromNumber:[NSNumber numberWithDouble:value]];
}

- (void)removeNull:(UILabel *)label {
    label.text = [ label.text stringByReplacingOccurrencesOfString:VALUE_NULL withString:VALUE_BREAK_LINE];
}

- (BOOL)connected {
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    if ([reach isReachable]) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


@end
