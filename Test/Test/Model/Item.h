//
//  Item.h
//  TestObjC
//
//  Created by Rene Cabañas on 17/11/20.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject

@property (nonatomic, strong) NSString *idItem;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lng;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *crossStreet;
@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *categories_name;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *reference;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *likes;
@property (nonatomic, strong) NSString *rating;
@property (nonatomic, strong) NSString *shortUrl;


-(id) initWhitData:(NSDictionary *)item;

@end

