//
//  Global.h
//  Test
//
//  Created by Admin on 17/11/20.
//  Copyright © 2020 Rene Cabañas. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Global)
+ (NSString *)stringWithDistance:(double)distance;
+ (NSString *)stringWithDouble:(double)value;
+ (void)removeNull:(NSString *)labelText;
+ (BOOL)connected;
@end

NS_ASSUME_NONNULL_END
