//
//  Utils.h
//  Test
//
//  Created by Admin on 17/11/20.
//  Copyright © 2020 Rene Cabañas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Utils : NSObject
- (NSString *)stringWithDistance:(double)distance;
- (NSString *)stringWithDouble:(double)value;
- (void)removeNull:(UILabel *)label;
- (BOOL)connected;
@end

NS_ASSUME_NONNULL_END
