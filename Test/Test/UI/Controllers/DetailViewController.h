//
//  DetailViewController.h
//  Test
//
//  Created by Rene Cabañas on 17/11/20.
//


#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "Request.h"
#import "UIImageView+AFNetworking.h"
#import "Item.h"

@interface DetailViewController : UIViewController <RequestProtocol, UIWebViewDelegate>{
    
}

@property (weak, nonatomic) IBOutlet UIImageView *imageFull;
@property (weak, nonatomic) IBOutlet UILabel *labelname;
@property (weak, nonatomic) IBOutlet UILabel *labeldistance;
@property (weak, nonatomic) IBOutlet UILabel *labeladress;
@property (weak, nonatomic) IBOutlet UILabel *labellikes;
@property (weak, nonatomic) IBOutlet UILabel *labelrating;
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property(strong,nonatomic) IBOutlet WKWebView *webview;

@property (strong, nonatomic) NSString *idItem;

@end
