//
//  ViewController.h
//  Test
//
//  Created by Rene Cabañas on 17/11/20.
//



#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Request.h"
#import "Item.h"
#import "ItemViewCell.h"

@interface ViewController : UIViewController  <RequestProtocol, MKMapViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate> {
    CLLocationManager *locationManager;
    BOOL isSearching;
}

@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) CLLocation *currentLocation;

@property (weak, nonatomic) IBOutlet UITableView     *tableView;
@property (weak, nonatomic) IBOutlet MKMapView       *mkmapView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnoption;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnlocation;
@property (strong, nonatomic) UISearchBar *searchBar;

@property (nonatomic, retain) NSMutableArray *itemsArray;
@property (nonatomic, retain) NSMutableArray *filtereditemsArray;

@end

