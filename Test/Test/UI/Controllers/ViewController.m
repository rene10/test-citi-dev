//
//  ViewController.m
//  Test
//
//  Created by Rene Cabañas on 17/11/20.
//

#import "ViewController.h"
#import "Utils.h"
#import "DetailViewController.h"

static double const conts_radius_center = 0.0500;

@interface ViewController () 

@end

@implementation ViewController

@synthesize filtereditemsArray, itemsArray, tableView, mkmapView, btnoption, btnlocation, searchBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Museos cerca de mi";
    
    [self configureMap];
    [self configureTable];
    self.itemsArray = [[NSMutableArray alloc] init];
    self.filtereditemsArray = [[NSMutableArray alloc] init];
    [self.tableView setHidden:YES];
    [self.mkmapView setHidden:NO];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    CGPoint contentOffset = self.tableView.contentOffset;
    contentOffset.y += CGRectGetHeight(self.tableView.tableHeaderView.frame);
    self.tableView.contentOffset = contentOffset;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

-(void)didReceiveMemoryWarning{
    
}

- (IBAction)actionView:(id)sender {
    if(self.mkmapView.isHidden){
        [self.mkmapView setHidden:NO];
        [self.tableView setHidden:YES];
        [self.btnlocation setEnabled:YES];
        [self.btnlocation setTintColor:[UIColor whiteColor]];
        [self.btnoption setImage:[UIImage systemImageNamed:@"list.bullet"]];
    }else{
        [self.mkmapView setHidden:YES];
        [self.tableView setHidden:NO];
        [self.btnlocation setEnabled:NO];
        [self.btnlocation setTintColor:nil];
        [self.btnoption setImage:[UIImage systemImageNamed:@"map"]];
    }
}

- (void)setErrorList:(NSError *)error {
    NSLog(@"error %@", error);
}

-(void)setResponseList:(NSDictionary *)response {
    NSDictionary *object = [response objectForKey:@"response"];
    NSMutableArray *itemsArrayTemp = [[NSMutableArray alloc] init];
    itemsArrayTemp = [object objectForKey:@"venues"];
    
    for (int i = 0; i < itemsArrayTemp.count; i++){
        Item *item = [[Item alloc] initWhitData:itemsArrayTemp[i]];
        [self.itemsArray addObject:item];
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        
        CLLocationDegrees lat = [item.lat doubleValue];
        CLLocationDegrees lng = [ item.lng doubleValue];

        CLLocationCoordinate2D coordinate;
        coordinate.latitude = lat;
        coordinate.longitude = lng;

        annotation.coordinate = coordinate;
        annotation.title = item.name;
        annotation.subtitle = item.categories_name;
        
        MKAnnotationView *pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
        pinView.canShowCallout = YES;
        
        [self.mkmapView addAnnotation:annotation];
    }
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES];
    [self.itemsArray sortUsingDescriptors:@[sortDescriptor]];
    
    [self.tableView reloadData];
}

-(void) configureTable {
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0., 0., 320., 44.)];
    self.searchBar.delegate = self;
    self.tableView.tableHeaderView = searchBar;
}

-(void)configureMap{
    mkmapView.delegate = self;
    mkmapView.showsUserLocation = YES;
    if ([CLLocationManager locationServicesEnabled]){
        locationManager = [[CLLocationManager alloc] init];
        [locationManager requestAlwaysAuthorization];
        [locationManager requestWhenInUseAuthorization];
        [locationManager startUpdatingLocation];
    }
    
    self.currentLocation = mkmapView.userLocation.location;
    if(self.currentLocation != nil){
        [mkmapView setCenterCoordinate:self.currentLocation.coordinate animated:YES];
        NSString *coord =  [NSString stringWithFormat:@"%f,%f",self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude];
        [self callData:coord];
    }
}

- (IBAction)selectorsenderzoomToUserLocation:(id)sender{
    MKCoordinateRegion mapRegion;
    mapRegion.center = mkmapView.userLocation.coordinate;
    mapRegion.span.latitudeDelta = conts_radius_center;
    mapRegion.span.longitudeDelta = conts_radius_center;

    [mkmapView setRegion:mapRegion animated: YES];
}

-(void)callData:(NSString *)coord{
    Request *resquest = [[Request alloc] init];
    resquest.delegate = self;
    Utils *utils = [[Utils alloc] init];
    if([utils connected]){
        [resquest callServciceList:coord];
    }
}

-(void) actionDetail:(Item *)item {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"  bundle:nil];
    DetailViewController *detailViewController =  [storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
    detailViewController.idItem = item.idItem;
    [self presentViewController:detailViewController animated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isSearching) {
        return self.filtereditemsArray.count;
    }else{
        return self.itemsArray.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"ItemViewCell";
    ItemViewCell *cell = (ItemViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ItemViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    Item *item;
    if (isSearching) {
        item = [self.filtereditemsArray objectAtIndex:indexPath.row];
        [cell setItem:item];
    }else{
        item = [self.itemsArray objectAtIndex:indexPath.row];
        [cell setItem:item];
    }
    UIView *selectViewGB = [[UIView alloc] init];
    selectViewGB.frame = cell.frame;
    selectViewGB.backgroundColor = [UIColor grayColor];
    cell.selectedBackgroundView = selectViewGB;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 136;
}


-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Item *item;
    if (isSearching) {
        item = [self.filtereditemsArray objectAtIndex:indexPath.row];
    }else{
        item = [self.itemsArray objectAtIndex:indexPath.row];
    }
    [self actionDetail:item];
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    [self.mkmapView setCenterCoordinate:userLocation.coordinate animated:YES];
    NSString *coord =  [NSString stringWithFormat:@"%f,%f",userLocation.coordinate.latitude, userLocation.coordinate.longitude];
    self.currentLocation = userLocation.location;
    [self callData:coord];
    
    float spanX = conts_radius_center;
    float spanY = conts_radius_center;
    MKCoordinateRegion region;
    region.center.latitude = self.mkmapView.userLocation.coordinate.latitude;
    region.center.longitude = self.mkmapView.userLocation.coordinate.longitude;
    region.span.latitudeDelta = spanX;
    region.span.longitudeDelta = spanY;
    [self.mkmapView setRegion:region animated:YES];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    Item *temp;
    for (int i = 0; i < self.itemsArray.count; i++){
        temp = [self.itemsArray objectAtIndex:i];
        if([view.annotation.title isEqualToString:temp.name]){
            NSLog(@"pin %@",view.annotation.title);
        }
    }
    [self actionDetail:temp];

}

- (void)searchTableList {
    NSString *searchString = self.searchBar.text;
    for (Item *tempItem in self.itemsArray) {
        NSString *tempStr = tempItem.name;
        if([tempStr containsString:searchString]){
            [filtereditemsArray addObject:tempItem];
        }
    }
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES];
    [self.filtereditemsArray sortUsingDescriptors:@[sortDescriptor]];
    
    [self.tableView reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    isSearching = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [filtereditemsArray removeAllObjects];
    if([searchText length] > 2) {
        isSearching = YES;
        [self searchTableList];
    }
    else {
        isSearching = NO;
        [self.tableView reloadData];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchTableList];
}

@end
