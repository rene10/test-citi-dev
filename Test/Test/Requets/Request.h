//
//  Request.h
//  Test
//
//  Created by Rene Cabañas on 17/11/20.
//


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol RequestProtocol <NSObject>
@required
- (void) setResponseList:(NSDictionary *) response;
- (void) setErrorList:(NSError *) error;
@end

@interface Request : NSObject
-(void) callServciceItemDetail:(NSString *)idItem;
-(void) callServciceList:(NSString *)coord;
@property (nonatomic, weak) id<RequestProtocol> delegate;
@end

NS_ASSUME_NONNULL_END
