//
//  Item.m
//  TestObjC
//
//  Created by Rene Cabañas on 17/11/20.
//

#import "Item.h"

@implementation Item

-(id) initWhitData:(NSDictionary *) item {
    self = [super init];
    if(self){
        self.idItem = [item valueForKey:@"id"];
        self.name = [item valueForKey:@"name"];
        id location = [item objectForKey:@"location"];
        self.lat = [location valueForKey:@"lat"];
        self.lng  = [location valueForKey:@"lng"];
        self.address = [location valueForKey:@"address"];
        self.crossStreet  = [location valueForKey:@"crossStreet"];
        self.postalCode = [location valueForKey:@"postalCode"];
        self.city = [location valueForKey:@"city"];
        self.state = [location valueForKey:@"state"];
        self.country = [location valueForKey:@"country"];
        self.distance = [location valueForKey:@"distance"];
        
        NSMutableArray *categories = [item objectForKey:@"categories"];
        id categorie = categories[0];
        self.categories_name = [categorie valueForKey:@"name"];
        id iconObj = [categorie valueForKey:@"icon"];
        self.icon =  [NSString stringWithFormat:@"%@88%@", [iconObj valueForKey:@"prefix"], [iconObj valueForKey:@"suffix"]];
        
        id bestPhoto = [item objectForKey:@"bestPhoto"];

        NSString *urlphoto =  [NSString stringWithFormat:@"%@720x720%@", [bestPhoto valueForKey:@"prefix"], [bestPhoto valueForKey:@"suffix"]];

        self.image = urlphoto;

        id likesObj = [item valueForKey:@"likes"];
        self.likes = [likesObj valueForKey:@"count"];

        self.rating = [item valueForKey:@"rating"];
        self.shortUrl = [item valueForKey:@"shortUrl"];
    }
    return self;
}
@end
