//
//  Request.m
//  Test
//
//  Created by Rene Cabañas on 17/11/20.
//



#import "Request.h"
#import "AFNetworking.h"

static NSString *const conts_endpoint = @"https://api.foursquare.com/v2/venues/";
static NSString *const conts_client_id = @"1R2320TXZVWN20YGWMXWWYXXIAZVXGX3LPTCHSQR2XPAJEFR";
static NSString *const conts_client_secret = @"AWIX0ATCETNWCAHVUQ5EO0OQFPQB34RPLGF4AVWIRGUUXDU1";
static NSString *const conts_version = @"20120609";
static NSString *const conts_radius_default = @"30000";
static NSString *const conts_category = @"4bf58dd8d48988d181941735"; //Museos

@implementation Request

@synthesize delegate;

-(void) callServciceList:(NSString *)coord {
    
    NSString *ll = coord;
    NSString *strUrl = [NSString stringWithFormat:@"%@search?client_id=%@&client_secret=%@&v=%@&ll=%@&radius=%@&categoryId=%@", conts_endpoint,conts_client_id, conts_client_secret,conts_version,ll,conts_radius_default,conts_category];

    NSURL *URL = [NSURL URLWithString:strUrl];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:URL sessionConfiguration:configuration];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:@"" parameters:nil headers:nil progress:^(NSProgress * _Nonnull uploadProgress){
        
    } success:^(NSURLSessionDataTask *task, id responseObject){
       [self.delegate setResponseList:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error){
       [self.delegate setErrorList:error];
    }];
}

-(void) callServciceItemDetail:(NSString *)idItem {
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@?client_id=%@&client_secret=%@&v=%@", conts_endpoint , idItem, conts_client_id, conts_client_secret,conts_version];

    NSURL *URL = [NSURL URLWithString:strUrl];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:URL sessionConfiguration:configuration];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:@"" parameters:nil headers:nil progress:^(NSProgress * _Nonnull uploadProgress){
        
    } success:^(NSURLSessionDataTask *task, id responseObject){
       [self.delegate setResponseList:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error){
       [self.delegate setErrorList:error];
    }];
}

@end
