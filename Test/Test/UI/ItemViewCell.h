//
//  ItemViewCell.h
//  TestObjC
//
//  Created by Rene Cabañas on 17/11/20.
//


#import <UIKit/UIKit.h>
#import "Item.h"
#import "UIImageView+AFNetworking.h"

@interface ItemViewCell : UITableViewCell {

}
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel     *labelName;
@property (weak, nonatomic) IBOutlet UILabel     *labelAddress;
@property (weak, nonatomic) IBOutlet UILabel     *labelLikes;
@property (weak, nonatomic) IBOutlet UILabel     *labelDistance;

- (void)setItem:(Item*)item;

@end
