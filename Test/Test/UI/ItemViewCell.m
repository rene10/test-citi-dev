//
//  ItemViewCell.m
//  TestObjC
//
//  Created by Rene Cabañas on 17/11/20.
//


#import "ItemViewCell.h"
#import "Utils.h"

@implementation ItemViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
     [super setSelected:selected animated:animated];
}

-(void)layoutSubviews{
    [super layoutSubviews];
}
    
- (void)setItem:(Item*)item{
    self.labelAddress.text = item.address;
    self.labelName.text = item.name;
    
    NSString *strAddressFull = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@", item.address, item.crossStreet , item.city , item.country , item.state ,item.postalCode];
    self.labelAddress.text = strAddressFull;
    Utils *utils = [[Utils alloc] init];
    [utils removeNull:self.labelAddress];
    self.labelDistance.text = [NSString stringWithFormat:@"%@", [utils stringWithDistance:[item.distance doubleValue]]];
    self.labelLikes.text = [NSString stringWithFormat:@"%@", item.categories_name];
    
    __weak typeof(self) weakSelf = self;
    NSURL *url = [NSURL URLWithString:item.icon];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    UIImage *placeholderImage = [UIImage imageNamed:@"placeholder"];
   
    [self.imageIcon setImageWithURLRequest:request
                          placeholderImage:placeholderImage
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       weakSelf.imageIcon.image = image;
                                   } failure:nil];
     
}

@end
