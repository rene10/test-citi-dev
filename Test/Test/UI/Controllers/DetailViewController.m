//
//  DetailViewController.m
//  Test
//
//  Created by Rene Cabañas on 17/11/20.
//

#import "DetailViewController.h"
#import "Utils.h"

@interface DetailViewController (){
    Utils *utils;
}

@end

@implementation DetailViewController

@synthesize imageFull, labelname, labeldistance, labeladress, labelrating, labellikes, webview, imageIcon;

- (void)viewDidLoad {
    [super viewDidLoad];
    utils = [[Utils alloc] init];
    [self callDataDetail:self.idItem];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

-(void)didReceiveMemoryWarning{
    
}

-(void)callDataDetail:(NSString *)idItem{
    Request *resquest = [[Request alloc] init];
    resquest.delegate = self;
    if([utils connected]){
        [resquest callServciceItemDetail:idItem];
    }
}

- (void)setErrorList:(NSError *)error {
    NSLog(@"error %@", error);
}

-(void)setResponseList:(NSDictionary *)response {
    NSDictionary *object = [response objectForKey:@"response"];
    NSDictionary *venue = [object objectForKey:@"venue"];
    Item *item = [[Item alloc] initWhitData:venue];
    
    self.labelname.text = item.name;
    self.labeldistance.text = [utils stringWithDistance:[item.distance doubleValue]];
    
    __weak typeof(self) weakSelfIcon = self;
    NSURL *urlIcon = [NSURL URLWithString:item.icon];
    NSURLRequest *requestIcon = [NSURLRequest requestWithURL:urlIcon];
    UIImage *placeholderIcon = [UIImage imageNamed:@"placeholder"];
   
    NSString *strAddressFull = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@", item.address, item.crossStreet , item.city , item.country , item.state ,item.postalCode];
    self.labeladress.text = strAddressFull;
    [utils removeNull:self.labeladress];
    
    self.labelrating.text = [NSString stringWithFormat:@"Rating %@", item.rating];
    self.labellikes.text = [NSString stringWithFormat:@"%@ Likes", item.likes];

    [utils removeNull:self.labelrating];
    [utils removeNull:self.labellikes];


    [self.imageIcon setImageWithURLRequest:requestIcon
                          placeholderImage:placeholderIcon
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        weakSelfIcon.imageIcon.image = image;
                                   } failure:nil];
    
    __weak typeof(self) weakSelf = self;
    NSURL *url = [NSURL URLWithString:item.image];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    UIImage *placeholderImage = [UIImage imageNamed:@"placeholder"];
   
    [self.imageFull setImageWithURLRequest:request
                          placeholderImage:placeholderImage
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       weakSelf.imageFull.image = image;
                                   } failure:nil];
    
    NSURL *nsurl=[NSURL URLWithString:item.shortUrl];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [self.webview loadRequest:nsrequest];

}

@end
